# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Quick summary

Esta es una app de prueba del test Openbank,
esta implementa el uso de la api Marvel
La aplicacion consiste de dos pantallas, **la pantalla principal**, el cual es un buscador de superheroes:
1. en esta pantalla el usuario tiene la opcion de buscar por nombre completo o por las primeras letras, en caso de que la barra este vacia, se usaran valores por defecto
2. la pantalla esta paginada, por lo que al momento de llegar al final de la lista, la app devuelve los siguientes 20 items

Al seleccionar un superheroes, se mostrara el **detalle del superheroes**:
1. en esta pantalla se mostrara la fecha de actualizacion, y una lista con los comics, eventos, series, libros donde este ha participado.
2. al final de la lista, estara recursos extras, el cual permiten acceder a paginas externas mediante el navegador


### How do I get set up? ###


1) acceder desde terminal a la carpeta donde esta el proyecto y ejecutar el comando pod install

```Terminal
    pod install
```
2) ejecutar en el emulador, o en caso de tener una cuenta desarrollador, cambiar el bundle del proyecto, por uno registrado en la plataforma de Apple.

### How the architecture works ###

Para la prueba y por razones de tiempo, se uso una version simplificada de la arquitectura MVVM (sin el componente reactivo, pero usando la filosofia de pase de datos mediante delegate)

el proyecto esta dividido en:

1) **Model**, los cuales tienes la informacion de negocio, y se encargan de mapear el json(gracias a la herramienta swiftyJSON)
2) **ViewModel**, clase la cual tiene la logica de negocio de una pantalla en concreta, es la intermediaria entre View y los modelos + servicios
3) **View**, son la parte visual de la app, en esta version, la instanciacion de componentes, y presentacion de datos se hace en esta capa
4) **Services**, son clases autonomas, que se encargan de una tarea en particular, implemente 3 servicios; router, data y network
    1.1) Router, se encarga  de gestionar la transicion entre dos vistas, tanto la animacion como el pase de datos
    1.2) Data, se encarga de negociar con network el pase de datos con el api, formateando la url de los endpoints, y pasando la informacion a los ViewModel
    1.3) Network, se encarga de configurar la consulta a backend(en este proyecto usando la herramienta Alamofire, para simplificar ciertos procesos)
