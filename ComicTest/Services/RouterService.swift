//
//  RouterService.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/10/20.
//

import UIKit

class RouterService {
    let viewController: UIViewController
    init(with viewController: UIViewController) {
        self.viewController = viewController
    }
    
   
    
    func goToDetail(with character: Character){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        vc.modalPresentationStyle = .fullScreen
        vc.viewModel = DetailViewModel(character: character)
        viewController.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToMoreInfo(with uri: URL) {
        UIApplication.shared.open(uri)
    }
}
