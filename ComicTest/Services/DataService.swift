//
//  DataService.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/10/20.
//

import Foundation

typealias responsePaginated = ([Character], Int ,Bool)
typealias responseDetail = Character
typealias responseFail = NSError

/// Service that is in charge of managing the formatting of the url, and of acting as an intermediary between ViewModel and the api service
class DataService {
    
    weak var delegate : DataManagerDelegate?
    var permissionQuery: String {
        let timestamp = Date().currentTimeMillisToString()
        return "?ts=\(timestamp)&apikey=\(Constants.apiKey)&hash=\(Constants.generateHash(with: timestamp))"
    }
    
    func getCharacters(with search: String = "", type: TypeSearch? = nil, offset: Int = 0) {
        var url = Constants.urlBase + "/v1/public/characters" + permissionQuery + "&offset=\(offset)"
        if !search.isEmpty, let type = type {
            url += "&\(type.rawValue)=\(search)"
        }
        NetworkService.fetchPagedData(url: url, type: Character.self) { (result, hasMoreR) in
            self.delegate?.response((result, offset, hasMoreR), with: .get)
        } fail: { error in
            self.delegate?.response(error, with: .get)
        }

    }
    
    func getCharactersDetail(with id: Int) {
        let url = Constants.urlBase + "/v1/public/characters/\(id)" + permissionQuery
        NetworkService.fetchData(url: url, type: Character.self) { result in
            self.delegate?.response(result, with: .get)
        } fail: { error in
            self.delegate?.response(error, with: .get)
        }

    }
    
    
}
