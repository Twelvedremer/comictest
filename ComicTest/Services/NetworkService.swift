//
//  NetworkService.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/10/20.
//


import Alamofire
import SwiftyJSON

/// Service that provides methods to connect with the backend, validation and mapping of the query result
/// brings 3 methods, consult only arrangement, paged arrangement, single object
class NetworkService {
    
    static let manager: Session = {
        let configuration = URLSessionConfiguration.af.default
        configuration.timeoutIntervalForRequest = 90
        return Session(configuration: configuration)
    }()
    
    class func fetchArrayData<T>(url: String, type: T.Type, success:@escaping (_ result: [T]) -> Void, fail:@escaping (_ error: NSError)->Void) where T:JsonMappable {
        
        if isConnectedToNetwork() {
            let urlWithoutSpace = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? url
            manager.request(urlWithoutSpace, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                switch response.result {
                //Successful request
                case .success(let value):
                    //Validate that the request was OK
                    
                    guard let jsonBase = JSON(value)["data"]["results"].array,
                        let code = response.response?.statusCode,
                        200 ... 299 ~= code
                        else  {
                            fail(NSError())
                            return
                            
                    }
                    let data = jsonBase.compactMap{ T(json: $0)}
                    success(data)
                //Request denied by connection
                case .failure(let error):
                    fail(error as NSError)
                }
            }
        } else {
            fail(NSError())
        }
    }
    
    class func fetchPagedData<T>(url : String, type: T.Type, success:@escaping ([T], Bool) -> Void, fail:@escaping (_ error: NSError)->Void) where T:JsonMappable {
        
        if isConnectedToNetwork() {
            let urlWithoutSpace = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? url

            manager.request(urlWithoutSpace, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                switch response.result {
                //Successful request
                case .success(let value):
                    //Validate that the request was OK
                    guard let results =  JSON(value)["data"]["results"].array,
                          let offset =  JSON(value)["data"]["offset"].int,
                          let total =  JSON(value)["data"]["total"].int,
                        let code = response.response?.statusCode, 200 ... 299 ~= code
                        else{
                            fail(NSError())
                            return
                    }
                    
                    let pageArray = results.compactMap{ T(json: $0)}
                    let hasMorePage = (offset + Constants.limit) < total
                    success(pageArray, hasMorePage)
                //Request denied by connection
                case .failure(let error):
                    fail(error as NSError)
                }
            }
        } else {
            fail(NSError())
        }
    }
    
    class func fetchData<T>(defaultHeader: HTTPHeaders? = nil, url : String, body: NSDictionary? = nil, type: T.Type, success:@escaping (_ result: T) -> Void, fail:@escaping (_ error: NSError)->Void) where T:JsonMappable {
        
        if isConnectedToNetwork() {
            let urlWithoutSpace = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? url
            manager.request(urlWithoutSpace, method: .get, parameters: body as? Parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                switch response.result {
                //Successful request
                case .success(let value):
                    //Validate that the request was OK
                    guard let item = JSON(value)["data"]["results"].array?.first,
                          let data = T(json: item) else {
                            fail(NSError())
                            return
                    }
                    success(data)
                    
                //Request denied by connection
                case .failure(let error):
                    fail(error as NSError)
                }
            }
        } else {
            fail(NSError())
        }
    }
    
    
    static func isConnectedToNetwork() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
    
}

