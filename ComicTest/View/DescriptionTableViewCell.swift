//
//  DescriptionTableViewCell.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/11/20.
//

import UIKit

class DescriptionTableViewCell: UITableViewCell {
    static let nib = UINib(nibName: "DescriptionTableViewCell", bundle: nil)
    
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func viewDidLoad(with resource: ExternalResource){
        descriptionLabel.text = resource.name
        typeLabel.text = "See More"
        typeLabel.isHidden = false
    }
    
    func viewDidLoad(with resource: ComicResource){
        descriptionLabel.text = resource.name
        if !resource.type.isEmpty {
            typeLabel.text = "Type: " + resource.type
            typeLabel.isHidden = false
        } else {
            typeLabel.isHidden = true
        }
    }
    
}
