//
//  FeedHeaderTableViewCell.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/11/20.
//

import Kingfisher
import UIKit

class FeedDetailsDescriptionTableViewCell: UITableViewCell {
    static let nib = UINib(nibName: "FeedDetailsDescriptionTableViewCell", bundle: nil)

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var lastupdate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func viewDidLoad(with character: Character){
        thumbnailImageView.kf.setImage(with: character.thumbnail)
        descriptionLabel.text = !character.description.isEmpty ? character.description : "No information"
        lastupdate.text = "Last Update: " + character.date
    }

}
