//
//  DetailViewController.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/11/20.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    lazy var router = RouterService(with: self)
    var viewModel: DetailViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = viewModel.character.name
        viewModel.delegate = self
        tableView.register(FeedDetailsDescriptionTableViewCell.nib, forCellReuseIdentifier: "FeedDetailsDescriptionTableViewCell")
        tableView.register(DescriptionTableViewCell.nib, forCellReuseIdentifier: "DescriptionTableViewCell")
        viewModel.updateProfile()
    }

}



// MARK: UITableViewDataSource
extension DetailViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.detailSections.count
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch viewModel.detailSections[section] {
        case .header: return nil
        case .comics: return "Comics"
        case .events: return "Events"
        case .resource: return "Extras Resources"
        case .series: return "Series"
        case .stories: return "Stories"
        }
    }
    /// :nodoc:
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch viewModel.detailSections[section] {
        case .header: return 1
        case .comics: return viewModel.character.comics.count
        case .events: return viewModel.character.events.count
        case .resource: return viewModel.character.resource.count
        case .series: return viewModel.character.series.count
        case .stories: return viewModel.character.stories.count
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as! UITableViewHeaderFooterView).contentView.backgroundColor = UIColor.color(with: "2B3784")
        (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.white
    }
    
    /// :nodoc:
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch  viewModel.detailSections[indexPath.section]{
        case .header:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedDetailsDescriptionTableViewCell") as! FeedDetailsDescriptionTableViewCell
            cell.viewDidLoad(with: viewModel.character)
            cell.selectionStyle = .none
            return cell
        case .comics:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionTableViewCell") as! DescriptionTableViewCell
            cell.viewDidLoad(with: viewModel.character.comics[indexPath.row])
            cell.selectionStyle = .none
            return cell
        case .events:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionTableViewCell") as! DescriptionTableViewCell
            cell.viewDidLoad(with: viewModel.character.events[indexPath.row])
            cell.selectionStyle = .none
            return cell
        case .series:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionTableViewCell") as! DescriptionTableViewCell
            cell.viewDidLoad(with: viewModel.character.series[indexPath.row])
            cell.selectionStyle = .none
            return cell
        case .stories:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionTableViewCell") as! DescriptionTableViewCell
            cell.viewDidLoad(with: viewModel.character.stories[indexPath.row])
            cell.selectionStyle = .none
            return cell
        case .resource:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionTableViewCell") as! DescriptionTableViewCell
            cell.viewDidLoad(with: viewModel.character.resource[indexPath.row])
            cell.selectionStyle = .none
            return cell
        }

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewModel.detailSections[indexPath.section] == .resource {
            router.goToMoreInfo(with: viewModel.character.resource[indexPath.row].uri)
        }
    }
    
    /// :nodoc:
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension DetailViewController: DetailViewModelDelegate {
    func responseDetail(result: Character) {
        viewModel.character = result
        tableView.reloadData()
    }
}

