//
//  ViewController.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/10/20.
//

import UIKit
import UIScrollView_InfiniteScroll

class FeedViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var typeSearchField: UITextField!
    let viewModel = FeedViewModel()
    lazy var router = RouterService(with: self)
    
    func tooltips(with event: Selector?) -> UIToolbar{
        let toolBar = UIToolbar(frame: CGRect(x: 0,y: 0, width: 100, height: 100))
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = .red
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Buscar", style: .plain, target: self, action: event)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        return toolBar
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Hero Searchs"
        tableView.register(CharacterTableViewCell.nib, forCellReuseIdentifier: "CharacterTableViewCell")
        setTextFieldPicker()
        setInfityScroll()
        viewModel.delegate = self
        viewModel.getCharactersList()
    }
    
    func setInfityScroll() {
        let infiniteIndicator = UIActivityIndicatorView(style: .medium)
        infiniteIndicator.tintColor = .white
        tableView.infiniteScrollIndicatorView = infiniteIndicator
        tableView.addInfiniteScroll { _ in
            self.viewModel.getCharactersList(refresh: false)
        }
      
        tableView.setShouldShowInfiniteScrollHandler { _ -> Bool in
            return self.viewModel.haveMorePage
        }
    }
    
    func setTextFieldPicker() {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        searchBar.delegate = self
        typeSearchField.delegate = self
        typeSearchField.text = viewModel.typeSerch[0].rawValue
        typeSearchField.inputAccessoryView = tooltips(with: #selector(donePicker))
        searchBar.inputAccessoryView = tooltips(with: #selector(donePicker))
        typeSearchField.inputView = pickerView
    }
    
    @objc func donePicker(){
        self.view.endEditing(true)
        viewModel.getCharactersList()
    }
}


// MARK: UITableViewDataSource
extension FeedViewController: UITableViewDataSource{
        
    /// :nodoc:
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    /// :nodoc:
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CharacterTableViewCell") as! CharacterTableViewCell
            cell.viewDidLoad(with: viewModel.items[indexPath.row])
            cell.selectionStyle = .none
            return cell
    }
    
    /// :nodoc:
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


// MARK: UITableViewDelegate
extension FeedViewController: UITableViewDelegate{
    /// :nodoc:
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            router.goToDetail(with: viewModel.items[indexPath.row])
            return
        }
}


extension FeedViewController: FeedViewModelDelegate {
    func responsePaginated(result: [Character], offset: Int , hasNextPaged: Bool){
        viewModel.updateList(with: result, offset: offset, haveMorePage: hasNextPaged)
        tableView.reloadData()
    }
}


extension FeedViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel.typeSerch.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return viewModel.typeSerch[row].rawValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        viewModel.newSelectedTypeSearch = viewModel.typeSerch[row]
        typeSearchField.text = viewModel.typeSerch[row].rawValue
        viewModel.selectedTypeSearch = viewModel.typeSerch[row]
    }

}

extension FeedViewController: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == typeSearchField { return false }
        var updatedText = ""
        if let text = textField.text, let textRange = Range(range, in: text) {
            updatedText = text.replacingCharacters(in: textRange,
                                                   with: string)
        }
        viewModel.searchText = updatedText
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        viewModel.getCharactersList()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        viewModel.getCharactersList()
        return true
    }
    
    
}
