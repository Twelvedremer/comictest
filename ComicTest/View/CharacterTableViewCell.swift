//
//  CharacterTableViewCell.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/10/20.
//

import UIKit
import Kingfisher
class CharacterTableViewCell: UITableViewCell {
    static let nib = UINib(nibName: "CharacterTableViewCell", bundle: nil)
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func viewDidLoad(with character: Character){
        thumbnailImageView.kf.setImage(with: character.thumbnail)
        titleLabel.text = character.name
        descriptionLabel.text = !character.description.isEmpty ? character.description : "No information"
    }
    
}
