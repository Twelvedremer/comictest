//
//  DetailViewModel.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/10/20.
//

import Foundation

protocol DetailViewModelDelegate: DataManagerResponseDelegate {
    func responseDetail(result: Character)
}

enum DetailCharacterSection {
    case header
    case comics
    case series
    case events
    case stories
    case resource
}

class DetailViewModel {
    var delegate: DetailViewModelDelegate?
    let dataManager = DataService()
    var character: Character!
    var detailSections: [DetailCharacterSection] {
        var total = [DetailCharacterSection.header]
        if !character.comics.isEmpty { total.append(DetailCharacterSection.comics) }
        if !character.series.isEmpty { total.append(DetailCharacterSection.series) }
        if !character.events.isEmpty { total.append(DetailCharacterSection.events) }
        if !character.stories.isEmpty { total.append(DetailCharacterSection.stories) }
        if !character.resource.isEmpty { total.append(DetailCharacterSection.resource) }
        return total
    }
    
    init(character: Character) {
        self.character = character
        dataManager.delegate = self
    }
    
    func updateProfile() {
        dataManager.getCharactersDetail(with: character.id)
    }
    
}


extension DetailViewModel: DataManagerDelegate {
    func response<T>(_ data: T, with response: TypeRequest) {
        if let response = data as? Character {
            delegate?.responseDetail(result: response)
            return
        }
        
        if let response = data as? NSError {
            delegate?.responseError(with: response)
            return
        }
        delegate?.responseError(with: NSError())
    }
}
