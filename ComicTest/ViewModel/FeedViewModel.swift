//
//  FeedViewModel.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/10/20.
//

import Foundation

enum TypeSearch: String {
    case name
    case nameStartsWith
    case modifiedSince
    case comics
    case series
    case events
    case stories
}

protocol FeedViewModelDelegate: DataManagerResponseDelegate {
    func responsePaginated(result: [Character], offset: Int , hasNextPaged: Bool)
}

class FeedViewModel {
    var delegate: FeedViewModelDelegate?
    let dataManager = DataService()
    let typeSerch: [TypeSearch] = [.name, .nameStartsWith]
    
    // instance values
    var items: [Character] = []
    var searchText = ""
    var selectedTypeSearch: TypeSearch = .name
    var newSelectedTypeSearch: TypeSearch? = nil
    var haveMorePage: Bool = true
    
    
    init() {
        dataManager.delegate = self
    }
    
    /// Metodo para consultar la data
    /// - Parameter refresh: si es true, es porque se actualiza toda la lista, en caso de falso, solo se anexa al final
    func getCharactersList(refresh: Bool = true) {
        dataManager.getCharacters(with: searchText, type: selectedTypeSearch, offset: refresh ? 0 : items.count)
    }
    
    func updateList(with items: [Character], offset: Int, haveMorePage: Bool) {
        self.haveMorePage = haveMorePage
        if offset > 0 {
            self.items.append(contentsOf: items)
        } else {
            self.items = items
        }
    }
    
    
}

extension FeedViewModel: DataManagerDelegate {
    func response<T>(_ data: T, with response: TypeRequest) {
        if let response = data as? ([Character], Int ,Bool) {
            delegate?.responsePaginated(result: response.0, offset: response.1, hasNextPaged: response.2)
            return
        }
        
        if let response = data as? NSError {
            delegate?.responseError(with: response)
            return
        }
        delegate?.responseError(with: NSError())
    }
}




