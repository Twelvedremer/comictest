//
//  Constants.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/10/20.
//

import Foundation
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

class Constants {
    static var apiKey = "d5349b565b670863c4e6a844d3d7ebd5"
    static var urlBase = "http://gateway.marvel.com"
    static var limit = 20
    static fileprivate var privateKey = "23f7262bcb2d145dd5a5fed002c8395d4fbb1230"
    
    class func generateHash(with ts: String) -> String {
        let string = ts + Constants.privateKey + apiKey
        return string.MD5
        
    }
}
