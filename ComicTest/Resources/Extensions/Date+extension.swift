//
//  Date+extension.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/10/20.
//

import Foundation

extension Date {
    func currentTimeMillis() -> Int {
        return Int(self.timeIntervalSince1970 * 1000)
    }
    
    func currentTimeMillisToString() -> String {
        return "\(self.currentTimeMillis())"
    }
}
