//
//  JsonMapable.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/10/20.
//

import SwiftyJSON

/// With this protocol, it allows me to generalize the responses of the api, and manage the parsing
public protocol JsonMappable {
    init?(json: JSON)
}

