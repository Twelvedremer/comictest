//
//  DataManagerDelegate.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/10/20.
//

import UIKit

public enum TypeRequest{
    case post, get, patch, delete, error, deleteAll
}

/**
 Generic protocol that allows the connection between the networkDataservice and the modules that perform the queries
 */
public protocol DataManagerDelegate: class {
    func response<T>(_ data: T, with response: TypeRequest)
}



/// protocol to generalize the error message
 protocol DataManagerResponseDelegate {
    func responseError(with error: NSError)
 }


extension DataManagerResponseDelegate where Self: UIViewController{
    func responseError(with error: NSError) {
        let message = "Hubo un error con el servidor, intentelo mas tarde"
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ok", style: .default, handler: { _ in })
        alert.addAction(okAction)
        self.present(alert, animated: true)
    }
}
