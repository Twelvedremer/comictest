//
//  Character.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/10/20.
//

import SwiftyJSON

struct Character {
    var id: Int
    var name: String
    var description: String = ""
    var date: String
    var thumbnail: URL?
    var comics: [ComicResource] = []
    var series: [ComicResource] = []
    var events: [ComicResource] = []
    var stories: [ComicResource] = []
    var resource: [ExternalResource] = []
}

extension Character: JsonMappable{
  /// :nodoc:
  init?(json: JSON) {
    guard let id = json["id"].int, let name = json["name"].string else {
      return nil
    }
    self.id = id
    self.name = name
    self.description = json["description"].string ?? ""
    self.date = json["modified"].string ?? ""
    if let name = json["thumbnail"]["path"].string, let type = json["thumbnail"]["extension"].string {
        self.thumbnail = URL(string: name + "." + type)
    }
    
    self.comics = json["comics"]["items"].array?.compactMap({ item -> ComicResource? in
        return ComicResource(json: item)
    }) ?? []
    self.series = json["series"]["items"].array?.compactMap({ item -> ComicResource? in
        return ComicResource(json: item)
    }) ?? []
    self.events = json["events"]["items"].array?.compactMap({ item -> ComicResource? in
        return ComicResource(json: item)
    }) ?? []
    self.stories = json["stories"]["items"].array?.compactMap({ item -> ComicResource? in
        return ComicResource(json: item)
    }) ?? []
    self.resource = json["urls"].array?.compactMap({ item -> ExternalResource? in
        return ExternalResource(json: item)
    }) ?? []

  }
}

