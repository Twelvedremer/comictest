//
//  Comic.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/10/20.
//

import SwiftyJSON

struct ComicResource {
    var name: String
    var type: String
    var uri: URL
}


extension ComicResource: JsonMappable{
  init?(json: JSON) {
    guard let name = json["name"].string,
          let uriString = json["resourceURI"].string, let uri = URL(string: uriString) else {
      return nil
    }
    self.name = name
    self.type = json["type"].string ?? ""
    self.uri = uri
  }
}
