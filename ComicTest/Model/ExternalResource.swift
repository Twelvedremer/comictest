//
//  ExternalResource.swift
//  ComicTest
//
//  Created by jhonger delgado on 11/10/20.
//

import SwiftyJSON

struct ExternalResource {
    var name: String
    var uri: URL
}


extension ExternalResource: JsonMappable{
  init?(json: JSON) {
    guard let name = json["type"].string,
          let uriString = json["url"].string, let uri = URL(string: uriString) else {
      return nil
    }
    self.name = name
    self.uri = uri
  }
}

